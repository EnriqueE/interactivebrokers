## Pre Requisites

Please ensure you have the following pre requisites completed before running this code.

1. Install Java JDK 8
2. Install npm

---

## Run Project

1. From root folder, run **mvn clean package**
2. After war file has been successfully completed, head to the **target** folder and run **java -jar interview-0.0.1-SNAPSHOT.war**
3. In your browser, head to **localhost:8080**
4. You can then navigate between home, registration and login page
5. Create account in **Registration** page and then try logging in via **Login** page

## Final Steps
1. Extend Offer (: