package enrique.espinet.interview.controllers;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.*;

// Package imports
import enrique.espinet.interview.pojos.Users;

@RestController
@RequestMapping("/api")
public class RegistrationLogin {

    HashMap<String,Users> accounts = new HashMap<>();

    @PostMapping("/register")
    public String register(@RequestBody Users user){
        System.out.println(user);
        String registrationMessage = "";
        // Users newUser = new Users(emailAddress,password);
        try {
            if(accounts.containsKey(user.getEmailAddress())){
                registrationMessage = "400: Email already exists";
            }
            else{
                accounts.put(user.getEmailAddress(), user);
                registrationMessage = "Registration Complete";
            }
        }
        catch (Exception e) {
            registrationMessage = "400: Error, email already exists";
            return registrationMessage;
        }
            

        System.out.println(accounts.toString());

        return registrationMessage;
    }

    @PostMapping("/login")
    public String login(@RequestBody Users user){
        System.out.println(user);
        System.out.println(accounts.toString());
        String loginMessage = "";
        try {
            if(accounts.get(user.getEmailAddress()).getPassword().equals(user.getPassword())){
                loginMessage = "Login Complete";
            }
            else{
                loginMessage = "401: Unauthorized, please try again";
            }
        } catch (Exception e) {
            loginMessage = "401: Unauthorized, please try again";
        }
        return loginMessage;
    }

    @GetMapping("/ping")
    public String testEndpoint(){
        return "Backend successfully up and running";
    }
    
}
